# Fonctionnalités essentielles : 
================================

* **Initialisation du plateau de jeu**
	- **Mettre 4 pions dans chaque trou**
	
* **Déplacement des pions**
	- **Sur le clic d'un trou, rajouter +1 sur tous les trous suivant (sens antihoraire)**
	- **Ne pas pouvoir clicker sur une case avec 0 pions**

* **Boucle**
	- **Si l'incrémentation des points effectue un tour complet, on "saute" la case de départ**
	
* **Interdiction de jouer les pions adverses**
	- **Il faut par contre alterner les tours**
	
* **Prise des pions**
	- **Si dernière incrémentation est entre 2 et 3, on récupère les points**

* **Système de rafle ( prise des pions précédents )**
	- **Si les points ont été pris, on execute la même opération sur le trou précédent**
	- **on refait l'opération jusqu'a ce que l'affirmation soit fausse**

* **Interdiction d'obtenir des points dans son propre camp**
	
* Interdiciton d'affamer le joueur adverse
	- Le joueur est obligé de donner au moins un pion à son adversaire si celui ci n'a plus aucun pion
	
* **Conditions de fin de partie**
	- **nombre de pions =< 3**
	- **abandon d'un des joueurs**
	- **plus de coups à jouer pour l'un des joueurs**


# Fonctionnalités Secondaires : 
===============================

* Paramétrage du nom des joueurs

* Autres modes de jeu


# Fonctionnalités bonus : 
=========================

* **Scoring**

* IA pour jouer Solo
	

