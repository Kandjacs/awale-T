# Awale-Boy
===========

*Jeu d'awalé, réalisé par Corentin Viret et Kévin Gonzales*

## Technos utilisées :

* Html
* Css
* Javascript


## Fonctionnalités : 

* Mode de jeu à 2x6 trous, 4 graines par trou
* Déplacer ses graines au click sur un trou
* Séparation de la board pour chaque joueur
* Prise des graines dans les bonnes circonstances
* Prise des graines sur les cases précédentes dans les bonnes circonstances
* Système empêchant la prise dans son propre camps
* Système de points
    ### Conditions de fin de partie :
        - moins de 3 graines restants
        - un joueur n'a plus de graines
        - un joueur abandonne (Bouton B)


## Organisation : 

* Compréhension des règles du jeu
* Création d'un wireframe
* Mise en place de la roadmap
    - on détaille le plus possible chaque fonctionnalité pour bien la comprendre
    - on les mets dans un ordre logique nous permettant de paver notre progression
* Réalisation des deux premières features de la roadmap
    - on les fait en parallèle, les autres dépendant de celles-ci, on essaye de les finir au plus vite
    - réflexion est faite sur les avantages et inconvenients de la solution choisie
* On se sépare sur les features suivantes
    - On peu ainsi s'aider quand on bloque, échanger les idées, et avancer plus vite


# How To Play : 
===============

- Cloner le repo Git
- lancer le fichier game/index.html dans son navigateur
- Bouton B : abandonner et recommencer la partie




