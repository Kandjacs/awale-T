Projet Awalé
============
2021-01-25 projet javascript

Le projet se déroulera sur 2 semaines par binôme.


# Objectifs

* Découverte de Javascript
* Manipulation du DOM
* Gestion d'événements Javascript
* Approfondissement de l'algorithmie

Compétences :
* Compétence 1 : maquettage
* Compétence 2 : web statique
* Compétence 3 : web dynamique

# Tâches 

## Tâches demandées
* Réalisation d'une wireframe
* Développer un jeu awalé en JS (tour par tour pour 2 joueurs)

## Bonus

* Scoring
* IA pour jouer solo

# Organisation

## Etape 1

Les binômes définiront leur projet :
* maquettage
* organisation
* description de leur projet étape par étape (lister les fonctionnalités et les ordonner dans le temps)

## Etape 2

Réalisation des différentes fonctionnalités, une par une.

## Etape 3

Une fois les tâches demandées réalisées, définir des tâches optionnelles et les réaliser.

