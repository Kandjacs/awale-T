//Initialisation du tableau de jeu et de la partie 
let game = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]

let round = 0

let scorePlayer1 = 0
let scorePlayer2 = 0

// Fonction pour mettre à jour l'affichage

function updateDisplay(){
game.forEach((seeds, id) => document.getElementById(id).innerHTML=seeds);
document.getElementById("scores").innerHTML =`PLAYER 1 : ${scorePlayer1} (  ----  PLAYER 2 : ${scorePlayer2} (`  // Affichage du score
}

updateDisplay()
checkRound()

// Fonction pour alterner les joueurs

function nbRound(){round++} // fonction d'incrémentation pour avoir le numéro du round en cours

function checkRound(){  // fonction de vérification du round, et à quel joueur est le tour

  let j1t = document.getElementById("j1t")
  let j2t = document.getElementById("j2t")

  function listenerAdd(i){
    document.getElementById(i).parentNode.addEventListener('click', seeding)
  }
  function listenerRemove(i){
    document.getElementById(i).parentNode.removeEventListener('click', seeding)
  }

    if(round%2 ==0){ //rounds pairs

        for(let i = 6; i < 12; i++){
          j2t.textContent = "Player 2"; 
          listenerRemove(i)// Retrait Joueur 2
        }
        for(let i = 0; i < 6; i++){
          j1t.textContent = "[ Player 1"; 
          listenerAdd(i)// Ajout joueur 1
        }
      }
    else{            //rounds impairs
        for(let i = 0; i < 6; i++){
          j1t.textContent = "Player 1";  
          listenerRemove(i)// Retrait Joueur 1
        }
    
        for(let i = 6; i < 12; i++){
          j2t.textContent = "[ Player 2"; 
            listenerAdd(i)// Ajout joueur 2
          }
      }
    }


// Fonction pour égrainer

function seeding(event){
  let id = event.target.getAttribute("id");
  seeds =  game[id]

if(seeds > 0){ // Si il n'y a pas de graines dans le trou clické, afficher une alerte
    nbRound()
      for (let i = 0; i < seeds; i++){
        if (i==10){i++} // Si les pions font un tour complet, on saute la case de départ
        if (i==seeds-1){
          seedCapture(id, seeds, round);
        }
            game[(Number(id) + 1 + i) % 12]++
            game[id] = 0
            checkRound()
      }
    updateDisplay()
    canPlay()
  }
  else {
    alert("Veuillez ne pas jouer un trou ne comportant aucune graines !")
    checkRound()
  }
}


// Capture de points

function seedCapture(id, seeds, round){
  let lastTrou = (Number(id)+seeds) % 12

    if (game[lastTrou]<= 2 && game[lastTrou]>= 1 && isTrouPlayer(round, lastTrou) ){
      multipleCapture(id, seeds, round)
      let points = game[lastTrou]+1
      addPoints(points)
      game[lastTrou] = -1
    }
}


// fonction pour savoir à qui est le tour

function isTrouPlayer(round, trouId){
  if ((round % 2) == 0) {
    return trouId < 6 && trouId >= 0
  }
    return trouId >= 6  
}


// Fonction pour la rafle

function multipleCapture(id, seeds, round){
  let total = (Number(id) + seeds) % 12

for(let i = 0; i < seeds && isTrouPlayer(round, (total-i + 1)) ; i++ ){
  let total2 = total-i

  if(total-i <= -1){ 
    total2 = 12 - ((total-i)*-1) 
  }
  if (game[total2] <= 3){
      let points = game[total2]
    addPoints(points)
    game[total2] = 0
  }else break
}}


// Fonction pour ajouter les points au bon joueur

function addPoints(points){
  if (round % 2 == 0){
    scorePlayer2 += points
  }
  else{
    scorePlayer1 += points
  }
}


// Fonctions pour fin de partie 

// fin de partie (impossible de jouer)

function canPlay(){
  let player1 = game.slice(0, 6)
  let player1Total = 0
  
  let player2 = game.slice(6)
  let player2Total = 0

  let gameTotal = 0

  for(let i = 0; i < 11; i++) {
    gameTotal = game[i]
  }
  for(let i = 0; i < 6; i++) {
    player1Total += player1[i]
    player2Total += player2[i]
  }
  if ((player1Total <= 0 && round%2 == 0 ) || (player2Total <= 0 && round%2 == 1) || gameTotal <= 3 ){
    alert(`Score Player 1 :  ${scorePlayer1}  |  Score Player 2 :   ${scorePlayer2}`)
   game = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]
   updateDisplay()
  }
}

 // Bouton d'abandon
 document.getElementById("buttonB").addEventListener("click", forfeit);

 function forfeit(){
   game.forEach((seeds, id) => document.getElementById(id).innerHTML=4);
   alert( `Score Player 1 :  ${scorePlayer1}  |  Score Player 2 :   ${scorePlayer2}`);
 }